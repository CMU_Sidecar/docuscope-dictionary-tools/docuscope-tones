# DocuScope Tones

Tools for preprocessing a DocuScope language model's "tone" file to JSON to be consumed by CMU_Sidecar/docuscope-classroom> and CMU_Sidecar/docuscope-tag>.

## Administration and Support

For any questions regarding overall project or the language model used, please contact <suguru@cmu.edu>.

The project code is supported and maintained by the [Eberly Center](https://www.cmu.edu/teaching/) at [Carnegie Mellon University](www.cmu.edu). For help with this fork, project, or service please contact <eberly-assist@andrew.cmu.edu>.

## ds-tones

Generates JSON from a DocuScope dictionary `_tones.txt` file.
The `_tones.txt` file should have a format simmilar to the following:

```
CLUSTER: <ClusterName>
DIMENSION: <DimensionName>
LAT|LAT*|CLASS: <LatName>
```

This is essentially a flattened hierarchical structure where each CLUSTER
has one or more DIMENSION entries and each DIMENSION has one or more LAT listings
prefixed with `LAT:`, `LAT*:`, or `CLASS:`.
The `<ClusterName>` should correspond to the `name` field for clusters in the `common-dictionary.json`
used with CMU_Sidecar/docuscope-classroom> and `<LatName>` should refer to the LAT ids used
in the dictionary used with CMU_Sidecar/docuscope-tag>.  `<DimensionName>`s are not functionally used
in the Classroom or Tagger projects (though they are in other DocuScope projects) and must be unique.

See api/docuscope_tones_schema.json for the schema of the resulting JSON.

## Usage

These instructions assumes usage in a unix like environment.
It expects the _tones.txt file piped in as standard input and outputs the json to standard output.
1. Compile with [go-lang](https://go.dev/).
1. Execute `ds_tones < _tones.txt > tones.json`.
1. Compress 'tones.json` with [gzip](https://www.gzip.org/).

## Acknowledgements

This project was partially funded by the [A.W. Mellon Foundation](https://mellon.org/), [Carnegie Mello University](https://www.cmu.edu/)'s [Simon Initiative](https://www.cmu.edu/simon/) Seed Grant, and the [Berkman Faculty Development Fund](https://www.cmu.edu/proseed/proseed-seed-grants/berkman-faculty-development-fund.html).
